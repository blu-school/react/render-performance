# Video workshop


You can see a video explanation of the corresponding workshop [here](https://blubitoag-my.sharepoint.com/:v:/r/personal/p_ivanova_blubito_com/Documents/Aufnahmen/Front-end%20guild%20-%20Regular%20Meeting-20211005_110245-Meeting%20Recording.mp4?csf=1&web=1&e=EUzrr6)

# How to start

Switch to directory ./render-performance

Run `npm install`
Run `npm start`

# Problem introduction

After starting the application and clicking the "click me" button, you can see that react calls the render() method of the [NaiveSubComponent.tsx](src/GrumpySubComponent.tsx) multiple times. This isnt a big problem in such a small application but can lead to huge performance issues for more complex components (with their own child-components etc.).

## Why is this weird?

Naively we would expect that since the SubComponent isnt changed at all (no props or state are changed), it shouldnt get re-rendered at all. But Reacts default behaviour is a bit different.

React usually re-renders a component whenever its state changes - and a re-render of a component in turn also re-renders all child-components.

## Ok but... why is render called TWICE (or more) per state-change?

That has to do with the `<React.StrictMode>` that we've set in the index.tsx - in StrictMode, React first calls the render method "in a safe sandbox" to check for potential issues. Only if that check succeeds, it calls the render method again to really execute the change.

This is one of the reasons why we should always make sure that render() is a **pure function**, meaning it should not change anything outside of it so that it can be called multiple times always with the same result. 
This also means that the example code in [NaiveSubComponent.tsx](src/GrumpySubComponent.tsx) is bad! **Dont ever do this for real projects**

Additionally, react essentially doesnt guarantee you that it only calls render() once. There can be other reasons why react sometimes decides to call your render method again and again.

## Solution 1 - PureComponents

A pure component essentially implements the shouldComponentUpdate() function of reacts lifecycle with a default implementation that checks for **shallow changes** in state and props.
This might already be enough for many situations - but be careful! Since its a shallow check only, this means that a lot of cases are not covered and are a few pitfalls. For example:

- Adding an element to an array with not cause a re-render
- Changing a property of an object will not cause a re-render

You can see this at work in [PureSubComponent.tsx](src/PureSubComponent.tsx)

## Solution 2 - ShouldComponentUpdate

Instead of using the pre-defined shouldComponentUpdate from Reacts PureComponent, you can also implement your own.

You can see this at work in [GrumpySubComponent.tsx](src/GrumpySubComponent.tsx)
