import React from 'react';

interface SubComponentProps {
}

interface SubComponentState {
}

export default class NaiveSubComponent extends React.Component<SubComponentProps, SubComponentState> {

    private timesTheRenderMethodHasBeenCalled = 0;

    constructor(props: SubComponentProps) {
        super(props);
        this.state = {}
    }

    render() {
        this.timesTheRenderMethodHasBeenCalled = this.timesTheRenderMethodHasBeenCalled+1;

        return <div>
            <div>NaiveSubComponentRenders: {this.timesTheRenderMethodHasBeenCalled}</div>
        </div>;
    }
}