import React from 'react';

interface SubComponentProps {
}

interface SubComponentState {
}

export default class GrumpySubComponent extends React.Component<SubComponentProps, SubComponentState> {

    private timesTheRenderMethodHasBeenCalled = 0;

    constructor(props: SubComponentProps) {
        super(props);
        this.state = {}
    }

    shouldComponentUpdate(nextProps: SubComponentProps, nextState: SubComponentState) {
        return false; // Because f*** you, thats why.
    }

    render() {
        this.timesTheRenderMethodHasBeenCalled = this.timesTheRenderMethodHasBeenCalled+1;

        return <div>
            <div>GrumpySubComponentRenders: {this.timesTheRenderMethodHasBeenCalled}</div>
        </div>;
    }
}