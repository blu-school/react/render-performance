import React from 'react';

interface SubComponentProps {
}

interface SubComponentState {
}

export default class PureSubComponent extends React.PureComponent<SubComponentProps, SubComponentState> {

    private timesTheRenderMethodHasBeenCalled = 0;

    constructor(props: SubComponentProps) {
        super(props);
        this.state = {}
    }

    render() {
        this.timesTheRenderMethodHasBeenCalled = this.timesTheRenderMethodHasBeenCalled+1;

        return <div>
            <div>PureSubComponentRenders: {this.timesTheRenderMethodHasBeenCalled}</div>
        </div>;
    }
}