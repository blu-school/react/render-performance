import React, { useState } from 'react';
import './App.css';
import GrumpySubComponent from './GrumpySubComponent';
import NaiveSubComponent from './NaiveSubComponent';
import PureSubComponent from './PureSubComponent';

function App() {
  
  const [someNumber, setSomeNumber] = useState(Math.random());

  return (
    <div className="App">
      <header className="App-header" >
        Some number currently is: {someNumber}
        <NaiveSubComponent />
        <PureSubComponent />
        <GrumpySubComponent />
        <button onClick={() => setSomeNumber(Math.random())}>Click me</button>
      </header>
    </div>
  );
}

export default App;
